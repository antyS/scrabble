import copy


class Player:
    def __init__(self, name, conn) -> None:
        self.points = 0
        self.rack = []
        self.conn = conn
        self.name = name
        self.myTurn = False
    
    def makeCopyOfMe(self):
        new = Player(self.name, self.conn)
        new.points = copy.deepcopy(self.points)
        new.rack = copy.deepcopy(self.rack)
        new.conn = self.conn
        new.name = copy.deepcopy(self.name)
        new.myTurn = copy.deepcopy(self.myTurn)
        return new