class Letter(object):
    def __init__(self, letter: str="", points: int=0) -> None:
        super().__init__()
        self.letter = letter
        self.points = points
        self.isBlank = False
