from src.model import Letter


class Placement:
    def __init__(self, x: int=0, y: int=0, letter: Letter = Letter.Letter()) -> None:
        self.x = x
        self.y = y
        self.letter = letter.letter
        self.points = letter.points
        self.isBlank = letter.isBlank
