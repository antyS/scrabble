from src.model import Player


class CheckRequest:
    def __init__(self) -> None:
        pass


class PlaceRequest:
    def __init__(self, placed: list) -> None:
        self.placed = placed


class PassRequest:
    pass

class Response:
    def __init__(self,message:str, player:Player) -> None:
        self.message = message
        self.player = player
        
class HelloRequest:
    def __init__(self,name:str) -> None:
        self.name = name