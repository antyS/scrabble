# Copyright (c) 2016 Damian Rusinek - modified by Konrad Adamczyk
import json
import sys
import socket
import threading

from src.client.UI import BoardManager


def log(message: str):
    with open('klient.txt', 'a') as file:
        file.write(message)
    print(message)


def send_msg(sock, msg):
    dumps = json.dumps(msg)
    data = dumps.encode('utf-8')
    log("send:\n" + dumps)
    sock.sendall(data)


def handle_received(sock, manager: BoardManager):
    while True:
        try:
            dic = recv_msg(sock)
        except (EOFError, ConnectionError):
            handle_disconnect(sock)
            break
        manager.updateAll(dic)


def recv_msg(sock):
    data = sock.recv(409600)
    if not data:
        raise ConnectionError()
    decode = data.decode('utf-8')
    log("recv:\n" + decode)
    return json.loads(decode)


def handle_send(sock, q):
    while True:
        msg = q.get()
        if msg == None: break
        try:
            send_msg(sock, msg)
        except (ConnectionError, BrokenPipeError):
            handle_disconnect(sock)
            break


def handle_disconnect(sock):
    addr = sock.getpeername()
    log('Client {} disconnected'.format(addr))
    sock.close()


class Client:
    def __init__(self, manager: BoardManager, q, addr) -> None:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((addr, 8888))
        log('Connected to {}:{}'.format(addr, 8888))
        
        send_thread = threading.Thread(target=handle_send,
                                       args=[sock, q], daemon=True)
        recv_thread = threading.Thread(target=handle_received,
                                       args=[sock, manager], daemon=True)
        recv_thread.start()
        send_thread.start()
