import queue

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QPushButton, QInputDialog

from src.client.UI import BoardManager
from src.client.UI.components.tile import Tile
from src.client.UI.filters.BoardTileEventFilter import BoardTileEventFilter
from src.client.UI.filters.RackTileEventFilter import RackTileEventFilter
from src.client.net.client import Client
from src.model.Letter import Letter


class BoardWindow(QObject):
    def __init__(self, MainWindow, manager: BoardManager):
        super().__init__()
        MainWindow.setObjectName("Scrabble")
        MainWindow.resize(1073, 1000)
        self.currentAdded = []
        self.editable = False
        self.rackSelected = None
        self.isExchange = False
        self.toExchange = []
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.board = QtWidgets.QLabel(self.centralwidget)
        self.board.setGeometry(QtCore.QRect(50, 5, 780, 750))
        self.board.setText("")
        self.board.setPixmap(QtGui.QPixmap("scrabble-board.png"))
        self.board.setScaledContents(False)
        self.board.setObjectName("board")
        self.cells = [[None for x in range(16)] for y in range(16)]
        self.boardEventFilter = BoardTileEventFilter(manager)
        self.rackEventFilter = RackTileEventFilter(manager)
        
        xBase = self.board.x() + 53
        yBase = self.board.y() + 38
        for xi in range(16):
            yLine = yBase
            for yi in range(16):
                tile = self.cells[xi][yi] = Tile(self.centralwidget, xBase, yLine, xi, yi)
                tile.installEventFilter(self.boardEventFilter)
                tile.setTransparent()
                yLine += 45
            xBase += 45
        self.rack = []
        xBase = self.board.x() + 53 + 45 * 4
        yBase = self.board.y() + self.board.height()
        self.infoLabel = QtWidgets.QLabel(self.centralwidget)
        self.infoLabel.setGeometry(QtCore.QRect(self.board.x(), yBase + 45, 780, 200))
        self.scoringLabel = QtWidgets.QLabel(self.centralwidget)
        self.scoringLabel.setGeometry(QtCore.QRect(self.board.x() + self.board.width() + 25, self.board.y(), 100, 500))
        self.scrollArea = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea.setWidget(self.infoLabel)
        self.scrollArea.setGeometry(QtCore.QRect(self.board.x(), yBase + 45, 780, 200))
        self.infoLabel.setAlignment(QtCore.Qt.AlignTop)
        for i in range(1, 8):
            tile = Tile(self.centralwidget, xBase, yBase, i, i)
            tile.hide()
            tile.installEventFilter(self.rackEventFilter)
            self.rack.append(tile)
            xBase += 45
        x = self.board.x() + self.board.width() + 10
        y = self.board.y() + self.board.width() - 150
        self.submitBtn = self.addButton(x, y, "Dodaj")
        self.submitBtn.clicked.connect(manager.submitBtnClicked)
        y += 50
        self.changeBtn = self.addButton(x, y, "Zamień")
        self.changeBtn.clicked.connect(manager.exchangeBtnClicked)
        y += 50
        self.checkBtn = self.addButton(x, y, "Sprawdź")
        self.checkBtn.clicked.connect(manager.checkBtnClicked)
        y += 50
        self.passBtn = self.addButton(x, y, "Pasuj")
        self.passBtn.clicked.connect(manager.passBtnClicked)
        self.submitBtn.setDisabled(True)
        self.changeBtn.setDisabled(True)
        self.checkBtn.setDisabled(True)
        self.passBtn.setDisabled(True)
        
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1073, 31))
        self.menubar.setObjectName("menubar")
        self.menuO_aplikacji = QtWidgets.QMenu(self.menubar)
        self.menuO_aplikacji.setObjectName("menuO_aplikacji")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionZaliczenie = QtWidgets.QAction(MainWindow)
        self.actionZaliczenie.setObjectName("actionZaliczenie")
        self.menuO_aplikacji.addAction(self.actionZaliczenie)
        self.menubar.addAction(self.menuO_aplikacji.menuAction())
        
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
    
    def addButton(self, x: int, y: int, q: str):
        button = QPushButton(self.centralwidget)
        button.setGeometry(QtCore.QRect(x, y, 88, 44))
        button.setFlat(False)
        button.actions()
        button.setObjectName(q)
        button.setText(q)
        return button
    
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.menuO_aplikacji.setTitle(_translate("MainWindow", "O aplikacji"))
        self.actionZaliczenie.setText(_translate("MainWindow", "Zaliczenie Konrad Adamczyk"))


if __name__ == "__main__":
    import sys
    
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = QtWidgets.QMainWindow()
    q = queue.Queue()
    manager = BoardManager.BoardManager(q)
    ui = BoardWindow(mainWindow, manager)
    mainWindow.show()
    manager.setUi(ui)
    # Back up the reference to the exceptionhook
    sys._excepthook = sys.excepthook
    
    
    def my_exception_hook(exctype, value, traceback):
        print(exctype, value, traceback)
        sys._excepthook(exctype, value, traceback)
        sys.exit(1)
    
    
    sys.excepthook = my_exception_hook
    name = ""
    address = ""
    ok = False
    while not address:
        address, ok = QInputDialog.getText(mainWindow, 'Podaj adres serwera', 'Podaj adres serwera:',text="localhost")
    
    ok = False
    while not name:
        name, ok = QInputDialog.getText(mainWindow, 'Podaj swoje imie', 'Podaj swoje imie:')
    c = Client(manager, q, address)
    manager.sendHello(name)
    try:
        sys.exit(app.exec_())
    except:
        print("Exiting")
