from PyQt5 import QtCore, QtWidgets

from PyQt5.QtWidgets import QPushButton


from src.client.UI.vars import yellow, green
from src.model.Letter import Letter


class Tile(QPushButton):
    allowedLetters = ["Ó", "Ą", "Ć", "Ę", "Ł", "Ń", "Ś", "Ź", "Ż", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                      "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "W", "Y", "Z"]
    
    def __init__(self, parent, x, y, xi, yi, *__args):
        super().__init__(parent, *__args)
        self.boardX = xi
        self.boardY = yi
        self.setGeometry(QtCore.QRect(x, y, 44, 44))
        self.setFlat(False)
        self.actions()
        self.setObjectName(str(x) + str(y))
        self.label = QtWidgets.QLabel(parent)
        self.label.setGeometry(QtCore.QRect(x + 32, y + 27, 10, 15))
        self.label.setText(str(0))
        self.setText("Q")
        self.occupied = False
        self.setStyleSheet("background-color:rgb(%s)" % yellow)
        self.colorRgb = yellow
        self.rackFriend = None
        self.boardFriend = None
        self.transparent = True
        self.letter = Letter("",-1)
    
    def hide(self):
        super().hide()
        self.label.hide()
    
    def show(self):
        super().show()
        self.label.show()
    
    def fromLetter(self, letter: Letter):
        self.letter = letter
        if  letter.isBlank:
            self.setToolTip("wybierz literę za pomocą klawiatury")
        self.setText(str(letter.letter))
        self.label.setText(str(letter.points))
    
    def trySetSemiTransparent(self):
        if not self.transparent:
            self.setStyleSheet("background-color:rgba(%s,%d)" % (self.colorRgb, 200))
    
    def setGreenSemiTransparent(self):
        self.setStyleSheet("background-color:rgba(%s,%d)" % (green, 200))
        self.label.setStyleSheet("color:rgba(0,0,0,255)")
    
    def trySetOpaque(self):
        if not self.transparent:
            self.setOpaque()
    
    def setOpaque(self):
        self.setStyleSheet("background-color:rgba(%s,%d)" % (self.colorRgb, 255))
        self.label.setStyleSheet("color:rgba(0,0,0,255)")
    
    def setTransparent(self):
        self.setStyleSheet("background-color:rgba(0,0,0,0); color:rgba(0,0,0,0)")
        self.label.setStyleSheet("color:rgba(0,0,0,0)")
