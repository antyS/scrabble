from PyQt5 import QtCore
from PyQt5.QtCore import QObject

from src.client.UI import BoardManager
from src.client.UI.components.tile import Tile


class RackTileEventFilter(QObject):
    def __init__(self, manager: BoardManager, parent=None):
        super().__init__(parent)
        self.manager = manager
    
    def eventFilter(self, button: Tile, event):
        if event.type() == QtCore.QEvent.MouseButtonPress:
            self.manager.rackTileClicked(button)
            return True
        elif event.type() == QtCore.QEvent.ContextMenu:
            self.manager.rackTileRightClicked(button)
            return True
        if event.type() == QtCore.QEvent.HoverEnter:
            self.manager.rackTileHooverStart(button)
            return True
        elif event.type() == QtCore.QEvent.HoverLeave:
            self.manager.rackTileHooverEnd(button)
            return True
        elif event.type() == QtCore.QEvent.KeyPress:
            self.manager.tileKeyboardPressed(button, event)
            return True
        return False
