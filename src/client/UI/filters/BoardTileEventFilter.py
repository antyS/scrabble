from PyQt5 import QtCore
from PyQt5.QtCore import QObject

from src.client.UI.components.tile import Tile


class BoardTileEventFilter(QObject):
    
    def __init__(self,manager, parent=None):
        super().__init__(parent)
        self.manager = manager

    def eventFilter(self, button: Tile, event):
        if event.type() == QtCore.QEvent.HoverEnter:
            self.manager.boardTileHooverStart(button)
            return True
        elif event.type() == QtCore.QEvent.HoverLeave:
            self.manager.boardTileHooverEnd(button)
            return True
        elif event.type() == QtCore.QEvent.MouseButtonPress:
            self.manager.boardTileClicked(button)
            return True
        elif event.type() == QtCore.QEvent.ContextMenu:
            self.manager.boardTileRightClicked(button)
            return True
        elif event.type() == QtCore.QEvent.KeyPress:
            self.manager.tileKeyboardPressed(button, event)
            return True
        elif event.type() == QtCore.QEvent.FocusIn:
            self.manager.tileFocusIn(button)
            return True
        elif event.type() == QtCore.QEvent.FocusOut:
            self.manager.tileFocusOut(button)
            return True
        return False
