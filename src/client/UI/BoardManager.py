import copy

from src.client.UI.components.tile import Tile
from src.client.UI.scrabbleNotGenerated import BoardWindow
from src.client.UI.vars import yellow, orange, red
from src.model.Letter import Letter
from src.model.Placement import Placement


class BoardManager(object):
    def __init__(self, q) -> None:
        self.q = q
    
    def sendPass(self):
        self.q.put(
            {"operation": "pass"}
        )
    
    def sendHello(self, name: str):
        self.q.put(
            {"operation": "hello",
             "name": name
             }
        )
    
    def sendPlace(self, tiles: list):
        self.q.put(
            {"operation": "place",
             "placements": [(lambda x: x.__dict__)(Placement(tile.boardX, tile.boardY, tile.letter)) for tile in tiles]
             }
        )
    
    def sendCheck(self):
        self.q.put(
            {"operation": "check"}
        )
    
    def sendExchange(self, tiles: list):
        self.q.put(
            {"operation": "exchange",
             "letters": [t.letter.__dict__ for t in tiles]}
        )
    
    def drawBoard(self, board: list):
        for x, row in enumerate(board):
            for y, letter in enumerate(row):
                cell = self.ui.cells[x][y]
                if letter is not None:
                    cell.fromLetter(letter)
                    cell.transparent = False
                    cell.colorRgb = yellow
                    cell.setOpaque()
                    cell.rackFriend = cell
                else:
                    cell.transparent = True
                    cell.rackFriend = None
                    cell.colorRgb = yellow
                    cell.setTransparent()
    
    def updateRack(self, rack: list):
        for i, tile in enumerate(self.ui.rack):
            if i < len(rack):
                tile.fromLetter(rack[i])
                tile.show()
            else:
                tile.rackFriend = None
                tile.boardFriend = None
                tile.hide()
            tile.colorRgb = yellow
            tile.setOpaque()
    
    def updateScoring(self, me: dict, opponent: dict):
        self.ui.scoringLabel.setText(
            "Ja:\nimię:%s\npunktów:%d\n\nPrzeciwnik:\nimię:%s\npunktów:%d" %
            (me["name"], me["points"],
             opponent["name"], opponent["points"]))
    
    def updateAll(self, dic: dict):
        try:
            self.updateScoring(dic["me"], dic["opponent"])
        except KeyError:
            pass
        rack = []
        for r in dic["me"]["rack"]:
            l = Letter()
            l.__dict__ = r
            rack.append(l)
        self.updateRack(rack)
        board = []
        for row in dic["me"]["board"]:
            letterRow = []
            for w in row:
                cell = None
                if w is not None:
                    cell = Letter()
                    cell.__dict__ = w
                letterRow.append(cell)
            board.append(letterRow)
        
        self.drawBoard(board)
        self.addMessage(dic["message"])
        self.setButtons(dic["me"]["myTurn"], board)
    
    def addToRack(self, letter: Letter):
        added = False
        for r in self.ui.rack:
            if not r.isVisible():
                r.fromLetter(letter)
                r.show()
                added = True
                break
        if not added:
            raise Exception("rack is full")
    
    def submitBtnClicked(self):
        if not self.ui.editable:
            self.ui.editable = True
            self.allBtnDisabled()
            self.ui.submitBtn.setDisabled(False)
            self.ui.submitBtn.setText("Zatwierdź")
        else:
            self.ui.editable = False
            self.allBtnDisabled()
            self.ui.submitBtn.setText("Dodaj")
            self.sendPlace(self.ui.currentAdded)
    
    def boardTileClicked(self, button: Tile):
        if self.isPotential(button):
            self.ui.currentAdded.append(button)
            button.fromLetter(self.ui.rackSelected.letter)
            button.show()
            button.transparent = False
            button.trySetOpaque()
            self.ui.rackSelected.boardFriend = button
            button.rackFriend = self.ui.rackSelected
            button.colorRgb = yellow
            button.rackFriend.setOpaque()
            self.ui.rackSelected.hide()
            self.ui.rackSelected = None
            self.tileFocusIn(button)
    
    def boardTileRightClicked(self, button: Tile):
        if self.ui.editable and button.rackFriend is not None and button.rackFriend is not button:
            try:
                self.ui.currentAdded.remove(button)
            except ValueError:
                pass
            button.letter = Letter("", -1)
            button.transparent = True
            button.colorRgb = yellow
            button.setTransparent()
            button.rackFriend.show()
            button.rackFriend = None
    
    def setUi(self, ui: BoardWindow):
        self.ui = ui
    
    def boardTileHooverStart(self, button: Tile):
        if self.isPotential(button):
            letter = copy.deepcopy(self.ui.rackSelected.letter)
            letter.isBlank = False
            button.fromLetter(letter)
            button.show()
            button.setGreenSemiTransparent()
        else:
            button.trySetSemiTransparent()
    
    def boardTileHooverEnd(self, button: Tile):
        if self.isPotential(button):
            button.setTransparent()
        else:
            button.trySetOpaque()
    
    def rackTileRightClicked(self, button: Tile):
        if (self.ui.isExchange):
            self.ui.toExchange.append(button)
            button.colorRgb = red
            button.setOpaque()
    
    def rackTileClicked(self, button: Tile):
        if self.ui.editable:
            if self.ui.rackSelected is not None:
                self.ui.rackSelected.setOpaque()
            self.ui.rackSelected = button
            button.setGreenSemiTransparent()
        elif self.ui.isExchange:
            try:
                self.ui.toExchange.remove(button)
            except ValueError:
                pass
            button.colorRgb = yellow
            button.setOpaque()
    
    def passBtnClicked(self):
        self.allBtnDisabled()
        self.sendPass()
    
    def checkBtnClicked(self):
        self.allBtnDisabled()
        self.sendCheck()
    
    def exchangeBtnClicked(self):
        if not self.ui.isExchange:
            self.ui.isExchange = True
            self.allBtnDisabled()
            self.ui.changeBtn.setDisabled(False)
            self.ui.changeBtn.setText("Zatwierdź")
        else:
            self.ui.isExchange = False
            self.ui.changeBtn.setText("Zamień")
            self.allBtnDisabled()
            self.sendExchange(self.ui.toExchange)
    
    def rackTileHooverStart(self, button: Tile):
        if self.ui.editable:
            button.setGreenSemiTransparent()
    
    def rackTileHooverEnd(self, button: Tile):
        if self.ui.editable and self.ui.rackSelected is not button:
            button.setOpaque()
    
    def tileKeyboardPressed(self, button: Tile, event):
        if self.ui.editable and button.letter.isBlank:
            keyText = event.text().upper()
            if keyText in button.allowedLetters:
                button.letter.letter = keyText
                button.fromLetter(button.letter)
                if button.rackFriend:
                    button.rackFriend.fromLetter(button.letter)
    
    def tileFocusIn(self, button: Tile):
        if self.ui.editable and button.letter.isBlank:
            button.colorRgb = orange
            button.setOpaque()
    
    def tileFocusOut(self, button: Tile):
        if self.ui.editable and button.letter.isBlank:
            button.colorRgb = yellow
            button.setOpaque()
    
    def isPotential(self, button: Tile):
        return self.ui.editable \
               and self.ui.rackSelected is not None \
               and button.rackFriend is None \
               and self.isInLine(button)
    
    def isInLine(self, button):
        if not self.ui.currentAdded:
            return True
        first = self.ui.currentAdded[0]
        if len(self.ui.currentAdded) == 1:
            return button.boardX == first.boardX or button.boardY == first.boardY
        second = self.ui.currentAdded[1]
        return first.boardX == button.boardX if first.boardX == second.boardX else first.boardY == button.boardY
    
    def addMessage(self, message: str):
        self.ui.infoLabel.setText("%s\n%s" % (message, self.ui.infoLabel.text()))
    
    def setButtons(self, isMyTurn, board):
        if isMyTurn:
            self.ui.currentAdded = []
            self.ui.editable = False
            self.ui.isExchange = False
            self.ui.toExchange = []
            self.ui.rackSelected = None
            checkDisabled = False
            if board[7][7] is None:
                checkDisabled = True
            self.ui.checkBtn.setDisabled(checkDisabled)
            self.ui.changeBtn.setDisabled(False)
            self.ui.passBtn.setDisabled(False)
            self.ui.submitBtn.setDisabled(False)
        else:
            self.allBtnDisabled()
        self.ui.editable = False
        self.ui.rackSelected = None
    
    def allBtnDisabled(self):
        self.ui.checkBtn.setDisabled(True)
        self.ui.changeBtn.setDisabled(True)
        self.ui.passBtn.setDisabled(True)
        self.ui.submitBtn.setDisabled(True)
