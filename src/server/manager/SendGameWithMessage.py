from typing import Any


class SendGameWithMessage(Exception):
    def __init__(self,message:str, *args: Any) -> None:
        super().__init__(*args)
        self.message = message