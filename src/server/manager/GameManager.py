import random

import copy

from src.model import Player, Placement
from src.model.Letter import Letter
from src.model.request import PlaceRequest
from src.server.manager.SendGameWithMessage import SendGameWithMessage

bagTemplate = [(1, [("A", 9), ("E", 7), ("I", 8), ("N", 5), ("O", 6), ("R", 4), ("S", 4), ("W", 4), ("Z", 5)]),
               (2, [("C", 3), ("D", 3), ("K", 3), ("L", 3), ("M", 3), ("P", 3), ("T", 3), ("Y", 4)]),
               (3, [("B", 2), ("G", 2), ("H", 2), ("J", 2), ("Ł", 2), ("U", 2)]),
               (5, [("Ą", 1), ("Ę", 1), ("F", 1), ("Ó", 1), ("Ś", 1), ("Ż", 1)]),
               (6, [("Ć", 1)]),
               (7, [("Ń", 1)]),
               (9, [("Ź", 1)]),
               (0, [("", 2)])]
scoring = [list("T112111T111211T"),
           list("1D11131113111D1"),
           list("11D111212111D11"),
           list("211D1112111D112"),
           list("1111D11111D1111"),
           list("131113111311131"),
           list("112111212111211"),
           list("T112111D111211T"),
           list("112111212111211"),
           list("131113111311131"),
           list("1111D11111D1111"),
           list("211D1112111D112"),
           list("11D111212111D11"),
           list("1D11131113111D1"),
           list("T112111T111211T")]


def wordScore(x: int, y: int):
    char = scoring[x][y]
    result = 1
    if char == "T":
        result = 3
    elif char == "D":
        result = 2
    return result


def makeInitBag():
    bag = []
    for (points, list) in bagTemplate:
        for (char, count) in list:
            for i in range(0, count):
                letter = Letter(char, points)
                letter.isBlank = not char
                bag.append(letter)
    return bag


def isHorizontal(placed: list):
    yList = [p.y for p in placed]
    if len(set(iter(yList))) <= 1:
        return True
    return False


def isVertical(placed: list):
    xList = [p.x for p in placed]
    if len(set(iter(xList))) <= 1:
        return True
    return False


def letterScore(x: int, y: int):
    char = scoring[x][y]
    try:
        return int(char)
    except ValueError:
        return 1


def validateInlineAndSort(placed: list):
    xList = [p.x for p in placed]
    yList = [p.y for p in placed]
    if isVertical(placed):
        sorted(placed, key=lambda p: p.y)
        return
    if isHorizontal(placed):
        sorted(placed, key=lambda p: p.x)
        return
    raise SendGameWithMessage("Litery musza byc w jednej lini")


def isGoodWord(w: str):
    import urllib.request, urllib.parse
    w = urllib.parse.quote(w.encode("utf-8"))
    return urllib.request.urlopen(
        "http://www.pfs.org.pl/files/php/osps_funkcje.php?s=spr&slowo_arbiter=%s" % w) \
               .read() == b'1'


class GameManager(object):
    def __init__(self) -> None:
        self.bag = makeInitBag()
        random.shuffle(self.bag)
        self.passCount = 0
        self.board = [[None] * 15 for x in range(15)]
        self.lastPlacement = None
        self.lastState = None
        self.players = []
    
    def exchange(self, countToReturn: int, dontWantThis: list):
        random.shuffle(self.bag)
        result = self.bag[:countToReturn]
        del self.bag[:countToReturn]
        self.bag += dontWantThis
        random.shuffle(self.bag)
        return result
    
    def makeExchange(self, countToReturn: int, dontWantThis: list):
        for d in dontWantThis:
            for r in self.players[0].rack:
                if r.letter == d.letter and r.points == d.points:
                    self.players[0].rack.remove(r)
                    break
        
        self.players[0].rack += self.exchange(countToReturn, dontWantThis)
        self.nextTurn()
    
    def makePlacement(self, place: list):
        validateInlineAndSort(place)
        if self.board[7][7] is not None:
            self.validateTouches(place)
        else:
            self.validateAnyMiddle(place)
        self.validateContinous(place)
        self.lastState = self.makeCopyOfMe()
        self.lastPlacement = copy.deepcopy(place)
        self.players[0].points += self.calcPoints(place)
        self.place(place)
        self.completePlayerRack(place)
        self.nextTurn()
    
    def addPlayer(self, p: Player):
        self.players.append(p)
        if (len(self.players) < 2):
            raise SendGameWithMessage("czekaj na przeciwnika")
        else:
            self.startGame()
    
    def startGame(self):
        random.shuffle(self.players)
        self.players[0].myTurn = True
        for p in self.players:
            p.rack = self.exchange(7, [])
        raise SendGameWithMessage("Zaczyna gracz %s" % self.players[0].name)
    
    def nextTurn(self):
        self.silentNextTurn()
        raise SendGameWithMessage("Teraz gracz %s" % self.players[0].name)
    
    def endGame(self):
        unusedSum = 0
        for p in self.players:
            sum = 0
            for t in p.rack:
                sum += t.points
            p.points -= sum
            unusedSum += sum
        for p in self.players:
            if len(p.rack) == 0:
                p.points += unusedSum
        sorted(self.players, key=lambda p: p.points, reverse=True)
        raise SendGameWithMessage("gracz o imieniu " + self.players[0].name + " wygral, wylacz aplikacje")
    
    def makePass(self):
        self.passCount += 1
        if self.passCount >= 4:
            self.endGame()
        self.nextTurn()
    
    def completePlayerRack(self, place):
        for p in place:
            for pl in self.players[0].rack:
                if (p.letter == pl.letter and p.points == pl.points):
                    self.players[0].rack.remove(pl)
                    break
        for x in place:
            if len(self.bag) == 0:
                break
            self.players[0].rack.append(self.bag[0])
            self.bag = self.bag[1:]
    
    def validateTouches(self, placed: list):
        for p in placed:
            if self.touches(p):
                return
        raise SendGameWithMessage("Nowe slowa musza znajdowac sie obok juz istniejacych")
    
    def touches(self, p: Placement):
        if (p.x > 0 and self.board[p.x - 1][p.y] is not None):
            return True
        if (p.y > 0 and self.board[p.x][p.y - 1] is not None):
            return True
        if (p.x < 15 and self.board[p.x + 1][p.y] is not None):
            return True
        if (p.y < 15 and self.board[p.x][p.y + 1] is not None):
            return True
        return False
    
    def validateContinous(self, placed: list):
        if isVertical(placed):
            for y in range(placed[0].y, placed[-1].y + 1):
                if y in [p.y for p in placed]:
                    if self.board[placed[0].x][y] is not None:
                        raise SendGameWithMessage("Nie mozna klasc w miejsce gdzie cos juz stoi")
                else:
                    if (self.board[placed[0].x][y] is None):
                        raise SendGameWithMessage("ulożenie kostek musi tworzyc spojny ciag")
        if isHorizontal(placed):
            for x in range(placed[0].x, placed[-1].x + 1):
                if x in [p.x for p in placed]:
                    if self.board[x][placed[0].y] is not None:
                        raise SendGameWithMessage("Nie mozna klasc w miejsce gdzie cos juz stoi")
                else:
                    if self.board[x][placed[0].y] is None:
                        raise SendGameWithMessage("ulożenie kostek musi tworzyc spojny ciag")
    
    def calcPoints(self, placed: list):
        points = 0
        for p in placed:
            points += p.points * letterScore(p.x, p.y)
        for p in placed:
            points *= wordScore(p.x, p.y)
        if len(placed) == 7:
            points += 50
        return points
    
    def place(self, placed: list):
        for p in placed:
            self.board[p.x][p.y] = Letter(p.letter, p.points)
    
    def makeChallenge(self):
        words = []
        for p in self.lastPlacement:
            words += self.wordsPassingThrough(p)
        uniqueWords = list(set(words))
        for w in uniqueWords:
            if not isGoodWord(w):
                self.restoreLast()
                self.silentNextTurn()
                raise SendGameWithMessage("slowa %s nie ma w slowniku gracz %s traci runde" % (w, self.players[1].name))
        self.silentNextTurn()
        raise SendGameWithMessage("wszsytkie ze slow %s sa w slowniku" % uniqueWords)
    
    def silentNextTurn(self):
        if len(self.bag) <= 0 and (len(self.players[0].rack) <= 0 or (len(self.players[1].rack) <= 0)):
            self.endGame()
        self.players[0].myTurn = False
        self.players[1].myTurn = True
        tmp = self.players[0]
        self.players[0] = self.players[1]
        self.players[1] = tmp
    
    def wordsPassingThrough(self, p: Placement):
        words = []
        word = ""
        for x in range(15):
            if (self.board[x][p.y] is not None):
                word = word + self.board[x][p.y].letter
            else:
                if x < p.x:
                    word = ""
                else:
                    words.append(word)
        word = ""
        for y in range(15):
            if (self.board[p.x][y] is not None):
                word = word + self.board[p.x][y].letter
            else:
                if y < p.y:
                    word = ""
                else:
                    words.append(word)
        return [w for w in words if len(w) >= 2]
    
    def restoreLast(self):
        if self.lastState is not None:
            self.bag = self.lastState.bag
            self.passCount = self.lastState.passCount
            self.board = self.lastState.board
            self.lastPlacement = self.lastState.lastPlacement
            self.players = self.lastState.players
            self.lastState = self.lastState.lastState
    
    def makeCopyOfMe(self):
        new = GameManager()
        new.bag = copy.deepcopy(self.bag)
        new.passCount = copy.deepcopy(self.passCount)
        new.board = copy.deepcopy(self.board)
        new.lastPlacement = copy.deepcopy(self.lastPlacement)
        new.lastState = None
        new.players = [p.makeCopyOfMe() for p in self.players]
        return new
    
    def validateAnyMiddle(self, place):
        for p in place:
            if p.y == 7 and p.x == 7:
                return True
        raise SendGameWithMessage("Pierwszy ruch musi przechodzic przez srodek")
