# Copyright (c) 2016 Damian Rusinek - modified by Konrad Adamczyk
import json
import sys
import socket
import threading, queue

from src.model.Letter import Letter
from src.model.Placement import Placement
from src.model.Player import Player
from src.server.manager.GameManager import GameManager
from src.server.manager.SendGameWithMessage import SendGameWithMessage

clients = {}
lock = threading.Lock()
arbiter = []


def log(message: str):
    with open('serwer.txt', 'a') as file:
        file.writelines([message])
    print(message)


def getArbiter():
    if len(arbiter) == 0:
        arbiter.append(GameManager())
        return arbiter[0]
    else:
        result = arbiter[0]
        arbiter.remove(arbiter[0])
        return result


def recv_msg(sock):
    data = sock.recv(409600)
    if not data:
        raise ConnectionError()
    decode = data.decode('utf-8')
    log("server received from :{0}\n{1}".format(sock, decode))
    return json.loads(decode)


def send_msg(sock, msg):
    """ Send a string over a socket, preparing it first """
    log("server send to  :{0}\n{1}".format(sock, msg))
    data = msg.encode('utf-8')
    sock.sendall(data)


def sendToPlayer(p: Player, pArbiter: GameManager, message: str):
    toSend = {}
    me = {"name": p.name,
          "rack": [l.__dict__ for l in p.rack],
          "points": p.points,
          "myTurn": p.myTurn,
          "board": [[(p.__dict__ if p is not None else p) for p in row] for row in pArbiter.board]}
    if (len(pArbiter.players) > 1):
        opponent = {}
        secondPlayer = pArbiter.players[0] if pArbiter.players[0] != p else \
            pArbiter.players[1]
        opponent["points"] = secondPlayer.points
        opponent["name"] = secondPlayer.name
        toSend['opponent'] = opponent
    toSend["me"] = me
    toSend['message'] = message
    with lock:
        playerDic = clients[p.conn.fileno()]
        playerDic["queue"].put(json.dumps(toSend))


def handle_client_recv(sock):
    """ Receive messages from client and broadcast them to
      other clients until client disconnects """
    
    while True:
        try:
            dic = recv_msg(sock)
        except (EOFError, ConnectionError):
            handle_disconnect(sock)
            break
        client = clients[sock.fileno()]
        pArbiter = client['arbiter']
        try:
            if dic["operation"] == "hello":
                pArbiter.addPlayer(Player(dic["name"], sock))
            if dic["operation"] == "place":
                placements = []
                for pDict in dic["placements"]:
                    plac = Placement()
                    plac.__dict__ = pDict
                    placements.append(plac)
                pArbiter.makePlacement(placements)
            if dic["operation"] == "pass":
                pArbiter.makePass()
            if dic["operation"] == "check":
                pArbiter.makeChallenge()
            if dic["operation"] == "exchange":
                letters = []
                for lDict in dic["letters"]:
                    letter = Letter()
                    letter.__dict__ = lDict
                    letters.append(letter)
                pArbiter.makeExchange(len(letters), letters)
            else:
                SendGameWithMessage("nie obslugiwana operacja")
        
        except SendGameWithMessage as e:
            for p in pArbiter.players:
                message = "%s : %s" % (pArbiter.players[0].name, e.message)
                log(message)
                sendToPlayer(p, pArbiter, message)


def handle_client_send(sock, q):
    while True:
        msg = q.get()
        if msg == None: break
        try:
            send_msg(sock, msg)
        except (ConnectionError, BrokenPipeError):
            handle_disconnect(sock)
            break


def handle_disconnect(sock):
    fd = sock.fileno()
    with lock:
        # Get send queue for this client
        q = clients.get(fd, None)['queue']
        # If we find a queue then this disconnect has not yet
        # been handled
        if q:
            q.put(None)
            del clients[fd]
        
        addr = sock.getpeername()
        log('Client {} disconnected'.format(addr))
        sock.close()


if __name__ == '__main__':
    
    listen_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_sock.bind(("0.0.0.0", 8888))
    listen_sock.listen(5)
    
    addr = listen_sock.getsockname()
    log('Listening on {}'.format(addr))
    
    while True:
        client_sock, addr = listen_sock.accept()
        q = queue.Queue()
        clientArbiter = getArbiter()
        with lock:
            clients[client_sock.fileno()] = {
                'queue': q,
                'arbiter': clientArbiter
            }
        recv_thread = threading.Thread(target=handle_client_recv,
                                       args=[client_sock], daemon=True)
        send_thread = threading.Thread(target=handle_client_send,
                                       args=[client_sock, q], daemon=True)
        recv_thread.start()
        send_thread.start()
        log('Connection from {0}'.format(addr))
